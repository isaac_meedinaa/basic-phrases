package example.isaac.basicphrases;

import android.media.MediaPlayer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuInflater;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    Button btn1, btn2, btn3, btn4, btn5, btn6, btn7, btn8;
    MediaPlayer mediaPlayer1, mediaPlayer2, mediaPlayer3, mediaPlayer4,
            mediaPlayer5, mediaPlayer6, mediaPlayer7, mediaPlayer8;
    boolean clicked = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // MediaPlayer
        mediaPlayer1 = MediaPlayer.create(this, R.raw.doyouspeakenglish);
        mediaPlayer2 = MediaPlayer.create(this, R.raw.goodevening);
        mediaPlayer3 = MediaPlayer.create(this, R.raw.hello);
        mediaPlayer4 = MediaPlayer.create(this, R.raw.howareyou);
        mediaPlayer5 = MediaPlayer.create(this, R.raw.ilivein);
        mediaPlayer6 = MediaPlayer.create(this, R.raw.mynameis);
        mediaPlayer7 = MediaPlayer.create(this, R.raw.please);
        mediaPlayer8 = MediaPlayer.create(this, R.raw.welcome);
    }

    public void audioClick(View view) {
        switch (view.getId()) {
            case R.id.btn_one:
                mediaPlayer1.start();
                break;
            case R.id.btn_two:
                mediaPlayer2.start();
                break;
            case R.id.btn_three:
                mediaPlayer3.start();
                break;
            case R.id.btn_four:
                mediaPlayer4.start();
                break;
            case R.id.btn_five:
                mediaPlayer5.start();
                break;
            case R.id.btn_six:
                mediaPlayer6.start();
                break;
            case R.id.btn_seven:
                mediaPlayer7.start();
                break;
            case R.id.btn_eight:
                mediaPlayer8.start();
                break;
        }
    }
}
